package com.example.hello3.slice;

import com.example.hello3.ResourceTable;
import com.example.myjavalib.AlertDialog;
import com.example.myjavalib.User;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        User andy = new User("andy", 28);

        Text text = (Text) findComponentById(ResourceTable.Id_text_helloworld);
        text.setText(andy.getName());

        findComponentById(ResourceTable.Id_btn).setClickedListener(component -> {
            showAlertDialog("我是标题", "kotlin实现的Alert", "取消","确定", component1 -> {
                new ToastDialog(this).setText("点击了确定").setAlignment(LayoutAlignment.CENTER).show();
            });
        });
    }

    private void showAlertDialog(String title, String msg, String cancelTxt,
                                 String posTxt, Component.ClickedListener listener) {
        AlertDialog alertDialog = new AlertDialog(this, ResourceTable.Layout_dialog_alert,
                new AlertDialog.ComponentId(ResourceTable.Id_tvTitle, ResourceTable.Id_tvContent, ResourceTable.Id_tvCancel, ResourceTable.Id_tvSure));
        alertDialog.setTitle(title);
        alertDialog.setContent(msg);
        alertDialog.setCancel(cancelTxt, null);
        alertDialog.setSure(posTxt, listener);
        alertDialog.show();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
