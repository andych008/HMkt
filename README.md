## 先说一说kotlin
我们知道：
1. kotlin目前是安卓首选的编程语言。
   
   > 安卓逐渐抛弃java，拥抱kotlin这是大的趋势。
   
2. kotlin的最大优点就是与java的互操作性。

3. kotlin编译的产物和java一样是bytecode(不抬杠，本文只说面向jvm的kotlin)。

4. kotlin是一门现代高级语言。
   > java也是高级语言，但开发效率很低。
   >
   > kotlin作为一门现代的语言，语法更简洁，而且具有很多高级特性，比如：Null Safe、Data Class、扩展、操作符重载、lambda表达式、闭包等


## 第一次尝试

我使用的鸿蒙ide是DevEco Studio 3.0.0.800。要使用kotlin，肯定要把相应的gradle插件、ide插件等配置上。

1. gradle插件(作用:编译.kt文件)
   project的gradle.build中配置
   
   ```groovy
   buildscript {
       dependencies {
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10"
       }
   }
   ```
   
   entry的gradle.build中配置
   
   ```groovy
   apply plugin: 'kotlin'
   ```
   
2. ide插件(作用:让ide识别.kt文件、高亮显示、语法提示等)

   DevEco Studio 3.0.0.800是基于IntelliJ IDEA`212.5457.46`，所以我们找到相应的版本并下载https://plugins.jetbrains.com/plugin/6954-kotlin/versions/stable/150173 

   把下载的zip包直接拖拽到DevEco界面上就会自动安装。
   ![16410305581.jpg](https://harmonyos.oss-cn-beijing.aliyuncs.com/images/202201/81dc04570173e7f0ae72575e8828837b2d0770.jpg?x-oss-process=image/resize,w_468,h_649) 
3. 建一个kotlin的Data Class, User
   
   ```java
   data class User(val name: String, val age: Int)
   ```
   在MainAbilitySlice中使用User
   ```java
   public class MainAbilitySlice extends AbilitySlice {
       @Override
       public void onStart(Intent intent) {
           super.onStart(intent);
           super.setUIContent(ResourceTable.Layout_ability_main);
           User andy = new User("andy", 28);//使用Data Class ，就像java的类一样
   
           Text text = (Text) findComponentById(ResourceTable.Id_text_helloworld);
           text.setText(andy.getName());
       }
   }
   ```
   
4. 结果:

   能够识别.kt文件，并且语法高亮都没问题，说明ide插件配置成功。

   但是，编译失败，提示`The 'java' plugin has been applied, but it is not compatible with the Hap plugins.`。我的理解是，`org.jetbrains.kotlin:kotlin-gradle-plugin`和`com.huawei.ohos:hap`冲突。这个问题，已经超出了我的能力范围。

   有没有别的办法呢？

## 第二次尝试

既然`org.jetbrains.kotlin:kotlin-gradle-plugin`和`com.huawei.ohos:hap`冲突，我们再建一个java library的module，把.kt文件放在这个module下，是否可行呢？答案是：可行！

直接看源码吧----> https://gitee.com/andych008/HMkt

## 第三次尝试(kotlin代码中使用鸿蒙sdk中的类)

以自定义一个AlertDialog为例（参考https://gitee.com/chinasoft_ohos/HiPermission/blob/master/library/src/main/java/me/weyye/hipermission/AlertDialog.java）。纯java的代码使用kotlin来实现肯定是没问题的，如果依赖鸿蒙sdk的的类呢？比如`ohos.agp.window.dialog.CommonDialog`。

也很简单，直接把sdk中的ohos.jar(`D:\Huawei\Sdk\java\3.0.0.0\api\ohos.jar`)放在我的们MyJavaLib中，依赖这个类只是为了完成编译，不会打进最终的hap里。

但是编译过程生成的`ResourceTable`，我们只能以变通的方式来依赖了。以这个AlertDialog为例，布局文件的资源id就是一个int，其中的组件id也是int，因为id较多，我们封装到一个class ComponentId里。完整的代码如下：(把原java实现通过ide转成kotlin，然后简单修改。我对kotlin用得也不多，所以具体细节肯定有不符合kotlin的使用习惯。这篇文章的目的只是证明可行性，并给出执行方案)

```java
package com.example.myjavalib
import ohos.agp.components.Component
import ohos.agp.components.LayoutScatter
import ohos.agp.components.Text
import ohos.agp.utils.LayoutAlignment
import ohos.agp.window.dialog.CommonDialog
import ohos.agp.window.dialog.IDialog
import ohos.app.Context
import ohos.multimodalinput.event.KeyEvent

/**
 * 自定义提示弹窗
 *
 * @since 2021-04-12
 * https://gitee.com/chinasoft_ohos/HiPermission/blob/master/library/src/main/java/me/weyye/hipermission/AlertDialog.java
 */
class AlertDialog(context: Context?, resId: Int, componentId: ComponentId) : CommonDialog(context) {
    private var tvTitle: Text? = null
    private var tvContent: Text? = null
    private var tvCancel: Text? = null
    private var tvSure: Text? = null

    data class ComponentId(val tvTitle: Int, val tvContent: Int, val tvCancel: Int, val tvSure: Int)


    init {
        setTransparent(true)
        setAlignment(LayoutAlignment.CENTER)
        val contentView = LayoutScatter.getInstance(context)
            .parse(resId, null, true)
        contentCustomComponent = contentView
        initView(componentId)
        siteRemovable(false)
        siteKeyboardCallback { iDialog: IDialog?, keyEvent: KeyEvent? -> true }
    }

    private fun initView(componentId: ComponentId) {
        val component = contentCustomComponent
        tvTitle = component.findComponentById<Component>(componentId.tvTitle) as Text
        tvContent = component.findComponentById<Component>(componentId.tvContent) as Text
        tvCancel = component.findComponentById<Component>(componentId.tvCancel) as Text
        tvSure = component.findComponentById<Component>(componentId.tvSure) as Text

        tvCancel!!.clickedListener = Component.ClickedListener { hide() }
        tvSure!!.clickedListener = Component.ClickedListener { hide() }
    }

    /**
     * 设置标题
     *
     * @param title 标题
     */
    fun setTitle(title: String?) {
        tvTitle!!.text = title
    }

    /**
     * 设置内容
     *
     * @param content 内容
     */
    fun setContent(content: String?) {
        tvContent!!.text = content
    }

    /**
     * 设置取消按钮文字和点击事件监听
     *
     * @param cancel 按钮文字
     * @param listener 点击事件监听
     */
    fun setCancel(cancel: String?, listener: Component.ClickedListener?) {
        tvCancel!!.text = cancel
        tvCancel!!.clickedListener = Component.ClickedListener {
            hide()
            listener?.onClick(tvCancel)
        }
    }

    /**
     * 设置确定按钮文字和点击事件监听
     *
     * @param sure 按钮文字
     * @param listener 点击事件监听
     */
    fun setSure(sure: String?, listener: Component.ClickedListener?) {
        tvSure!!.text = sure
        tvSure!!.clickedListener = Component.ClickedListener {
            hide()
            listener?.onClick(tvSure)
        }
    }
}
```

**注意**：我们依赖的`ohos.jar`要和entry编译用的api level保持一致。



## 总结
虽然，我们经过尝试，可以在鸿蒙app开发中使用kotlin，但是，我还是不建议在实际中大量使用。说不准哪一天，DevEco Studio 中完全不支持kotlin了，那样之前写的代码维护会不太方便。但是，如果有一些平台不相关的代码，比如，在android和鸿蒙上都会使用，那么我们完全可以放心使用kotlin来实现。
另外，在华为开发者大会上（2021-10-22），华为表示，将发布自研编程语言。我们就暂时忍受一下难用的java吧。华为的自研编程语言，在特性上肯定会和kotlin、swift之类的现代语言一样。现在学习使用kotlin对以后一定会有用的！